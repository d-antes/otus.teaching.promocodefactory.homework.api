﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClient
{
    public class ClientFactory
    {
        public static IClient GetClient( string type, string address)
        {
            Dictionary<string, IClient> clients = new Dictionary<string, IClient>()
            {
                { "/signalr", new SignalrClient(address) },
                { "/grpc", new GrpcClient(address) },
                { "/graphql", new GraphqlClient(address) }
            };

            if (!clients.ContainsKey(type))
                throw new ArgumentException("wrong client name");

            return clients[type];
        }
    }
}
