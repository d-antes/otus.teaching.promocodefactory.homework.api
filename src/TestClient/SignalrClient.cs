﻿using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace TestClient
{
    internal class SignalrClient : IClient
    {
        private string _address;
        private HubConnection _connection;

        public SignalrClient(string address)
        {
            _address = address;
            _connection = new HubConnectionBuilder().WithUrl(_address).Build();
        }

        public async void CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            await _connection.InvokeAsync("CreateCustomer", request);
        }

        public async void DeleteCustomerAsync(Guid id)
        {
            await _connection.InvokeAsync("DeleteCustomer", id);
        }

        public async void EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            await _connection.InvokeAsync("EditCustomers", id, request);
        }

        public async void GetCustomersAsync()
        {
            await _connection.InvokeAsync("GetCustomers");
        }

        public async void GetCustomersById(Guid guid)
        {
            await _connection.InvokeAsync("GetCustomer", guid);

        }

        public void Run()
        {
            _connection.StartAsync();

            _connection.On("GetCustomers", (string data) =>
            {
                Console.WriteLine( data);
            });

            _connection.On("GetCustomer", (string data) =>
            {
                Console.WriteLine(data);
            });

            _connection.On("CreateCustomer", (string data) =>
            {
                Console.WriteLine(data);
            });

            _connection.On("EditCustomer", (string data) =>
            {
                Console.WriteLine(data);
            });

            _connection.On("DeleteCustomer", (string data) =>
            {
                Console.WriteLine(data);
            });

        }
    }
}