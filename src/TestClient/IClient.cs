﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClient
{
    public interface IClient
    {
        void Run();
        void GetCustomersAsync();
        void GetCustomersById(Guid guid); 
        void CreateCustomerAsync(CreateOrEditCustomerRequest request);
        void EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);
        void DeleteCustomerAsync(Guid id);
    }
}
