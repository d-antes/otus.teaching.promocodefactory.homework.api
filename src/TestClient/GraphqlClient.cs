﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace TestClient
{
    internal class GraphqlClient : IClient
    {
        private string _address;

        public GraphqlClient(string address)
        {
            _address = address;
        }

        public void CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            throw new NotImplementedException();
        }

        public void DeleteCustomerAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public void EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            throw new NotImplementedException();
        }

        public void GetCustomersAsync()
        {
            throw new NotImplementedException();
        }

        public void GetCustomersById(Guid guid)
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}